Synchronous wrapper for Jira REST API
=====================================

Jira REST API on ~~steroids~~ fibers.

Installation
------------
```
npm install jira-sync --save
```

Usage
-----
```
const URL = 'https://jira.example.com/jira';
const JiraSync = require('jira-sync');

JiraSync.run(URL, main);

function main(jira) {
    const myself = jira.rest('api/2/myself');
    console.log(myself);
}
```
