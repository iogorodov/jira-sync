const path = require('path');
const JiraSync = require('../../jira-sync');

const argv = process.argv.slice(2);
if (argv.length !== 2) {
    console.log('Usage: runner.js <script> <url>');
} else {
    const app = require(path.join(__dirname, argv[0]));
    JiraSync.run(argv[1], app);
}
