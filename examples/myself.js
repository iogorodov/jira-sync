const URL = 'https://jira.example.com/jira';
const JiraSync = require('../../jira-sync');

JiraSync.run(URL, main);

function main(jira) {
    const myself = jira.rest('api/2/myself');
    console.log(myself);
}
