const request = require('request');
const Fiber = require('fibers');
const Future = require('fibers/future');
const url = require('url');

const REST_PATH = 'rest/';

class JiraSync {
    constructor(options) {
        let parsed = {};
        if (typeof options === 'string') {
            parsed = url.parse(options);
        } else if (typeof options === 'object') {
            if (options.server) {
                parsed = url.parse(options.server);
            } else if (options.host) {
                parsed.protocol = options.protocol || 'https:';
                parsed.host = options.host;
                parsed.pathname = options.pathname || '';
            }
        }

        this._origin = url.resolve(parsed.protocol + '//' + parsed.host + parsed.pathname, REST_PATH);

        this._auth = null;
        if (parsed.auth) {
            const auth = parsed.auth.split(':');
            this._auth = { user: auth[0], pass: auth.length > 1 ? auth[1] : '' };
        } else if (parsed.username) {
            this._auth = { user: parsed.username, pass: parsed.password };
        } else if (parsed.user) {
            this._auth = { user: parsed.user, pass: parsed.pass };
        } 
    }

    auth(username, password) {
        if (!username) {
            this._auth = null;
        } else {
            this._auth = { user: username, pass: password };
        }

        return this;
    }

    rest(method, path, data) {
        function parseDefaults(method, path, data) {
            const methodType = typeof method;
            const pathType = typeof path;
            const dataType = typeof data;

            if (pathType === 'undefined' && dataType === 'undefined') {
                return { method: 'GET', path: method, data: null };
            } else if (pathType === 'object' && dataType === 'undefined') {
                return { method: 'GET', path: method, data: path };
            } else if (pathType === 'string' && dataType === 'undefined') {
                return { method: method.toUpperCase(), path: path, data: null };
            } else {
                return { method: method.toUpperCase(), path: path, data: data };
            }
        }

        function buildQuery(data) {
            return Object.keys(data).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(data[k]));
        }

        const args = parseDefaults(method, path, data);
        if (args.method === 'GET' && args.data !== null) {
            const query = buildQuery(args.data);
            args.path = args.path + (args.path.indexOf('?') >= 0 ? '&' : '?') + query;
            args.data = null;
        } else if (args.method === 'POST' && args.data !== null) {
            args.data = JSON.stringify(args.data);
        }

        const options = {
            method: args.method, 
            url: url.resolve(this._origin, args.path),
            body: args.data, 
            headers: {}
        };

        if (this._auth) {
            options.auth = this._auth;
        }

        if (args.data) {
            options.headers['Content-Type'] = 'application/json';
        }

        const future = new Future;
        request(options, (err, response, body) => {
            if (err) {
                future.throw(err);
            } else {
                future.return(JSON.parse(body));
            }
        });

        return future.wait();
    }

    static run(options, callback) {
        const jira = new JiraSync(options);
        Fiber(() => { callback(jira); }).run();
    }
}

module.exports = JiraSync;
